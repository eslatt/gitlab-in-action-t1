package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {

       // TODO discord access thing
       
        String landing_page_color = "purple";
        String landing_page_message = "Greetings from GitLab!";

        String index_html = "";
        index_html += "<!doctype html>";
        index_html += "<html lang=\"en\">";
        index_html += "  <head>";
        index_html += "    <meta charset=\"utf-8\">";
        index_html += "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">";
        index_html += "    <title>Tanuki Tech</title>";
        index_html += "    <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap\" rel=\"stylesheet\">";
        index_html += "    <link rel=\"stylesheet\" href=\"https://eslatt.gitlab.io/demo-store/assets/css/bootstrap.min.css\" crossorigin=\"anonymous\">";
        index_html += "    <link rel=\"stylesheet\" href=\"https://eslatt.gitlab.io/demo-store/assets/css/all.min.css\" crossorigin=\"anonymous\">";
        index_html += "    <link rel=\"stylesheet\" href=\"https://eslatt.gitlab.io/demo-store/assets/css/style.css\" crossorigin=\"anonymous\">";
        index_html += "    <style type='text/css' media='screen'>";
        index_html += "      .ra {";
        index_html += "        color: orange;";
        index_html += "      }";
        index_html += "    </style>";
        index_html += "  </head>";
        index_html += "  <body class=\"form-login-body\">";
        index_html += "    <div class=\"container\">";
        index_html += "      <div class=\"row\">";
        index_html += "        <div class=\"col-lg-10 mx-auto login-desk\">";
        index_html += "          <div class=\"row\">";
        index_html += "            <div class=\"col-md-7 detail-box\" style=\"background-color:"+landing_page_color+";\">";
        index_html += "              <div class=\"detailsh ra\"><img class=\"help\" src=\"assets/images/help.webp\" alt=\"\">";
        index_html += "                <div class=\"ra\">";
        index_html += "                  <div style=\"font-size: 2rem;\">"+landing_page_message+"</div>";
        index_html += "                  <div id=\"me\"></div>";
        index_html += "                  <script>";
        index_html += "                    me = \"ICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPSJjb2xvcjpwdXJwbGU7Ij4gJm5ic3A7IDxkaXY+CiAgICAgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+UmV2aWV3IEFwcHMgYnJpbmcgdmFsdWUgdG8gYWxsIHN0YWtlaG9sZGVycy48L3N0cm9uZz4KICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiAmbmJzcDsgPGRpdj4KICAgICAgICAgICAgICAgICAgICAgICAgPHN0cm9uZz5EZXZlbG9wbWVudCBWYWx1ZTwvc3Ryb25nPgogICAgICAgICAgICAgICAgICAgICAgPC9kaXY+CiAgICAgICAgICAgICAgICAgICAgICA8ZGl2PgogICAgICAgICAgICAgICAgICAgICAgICA8aT4uLi5uYXR1cmUgd2lsbCBzb29uIG1ha2UgYSBmb29sIG91dCBvZiB5b3UgW2lmIHlvdSBza2lwIHN0ZXBzXTwvaT4gLSBSb2JlcnQgUGlyc2lnCiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gJm5ic3A7IDxkaXY+CiAgICAgICAgICAgICAgICAgICAgICAgIDxzdHJvbmc+QnVzaW5lc3MgRHJpdmVuIE91dGNvbWVzIFZhbHVlPC9zdHJvbmc+CiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4KICAgICAgICAgICAgICAgICAgICAgIDxkaXY+CiAgICAgICAgICAgICAgICAgICAgICAgIDxpPklzIHRoaXMgb3JhbmdlIHJlYWxseSB3aGF0IHlvdSBoYWQgZW52aXNpb25lZD88L2k+CiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4KICAgICAgICAgICAgICAgICAgICA8L2Rpdj4=\";";
        index_html += "                    document.getElementById(\"me\").innerHTML = atob(me)";
        index_html += "                  </script>";
        index_html += "                </div>";
        index_html += "              </div>";
        index_html += "            </div><div class=\"col-md-5 loginform\"><h4>Welcome Back</h4><p>Signin to your Account</p><div class=\"login-det\"><div class=\"form-row\">";
        index_html += "                  <label for=\"\">Username</label>";
        index_html += "                  <div class=\"input-group mb-3\">";
        index_html += "                    <div class=\"input-group-prepend\">";
        index_html += "                      <span class=\"input-group-text\" id=\"basic-addon1\">";
        index_html += "                        <i class=\"far fa-user\"></i>";
        index_html += "                      </span>";
        index_html += "                    </div>";
        index_html += "                    <input type=\"text\" class=\"form-control\" placeholder=\"Enter Username\" aria-label=\"Username\" aria-describedby=\"basic-addon1\">";
        index_html += "                  </div>";
        index_html += "                </div>";
        index_html += "                <div class=\"form-row\">";
        index_html += "                  <label for=\"\">Password</label>";
        index_html += "                  <div class=\"input-group mb-3\">";
        index_html += "                    <div class=\"input-group-prepend\">";
        index_html += "                      <span class=\"input-group-text\" id=\"basic-addon1\">";
        index_html += "                        <i class=\"fas fa-lock\"></i>";
        index_html += "                      </span>";
        index_html += "                    </div>";
        index_html += "                    <input type=\"password\" class=\"form-control\" placeholder=\"Enter Password\" aria-label=\"Username\" aria-describedby=\"basic-addon1\">";
        index_html += "                  </div>";
        index_html += "                </div>";
        index_html += "                <p class=\"forget\">";
        index_html += "                  <a href=\"\">Forget Password?</a>";
        index_html += "                </p>";
        index_html += "                <button class=\"btn btn-sm btn-danger\">Login</button>";
        index_html += "              </div></div>";
        index_html += "          </div>";
        index_html += "        </div>";
        index_html += "      </div>";
        index_html += "    </div>";
        index_html += "  </body>";
        index_html += "</html>";

        return index_html;
    }

}
